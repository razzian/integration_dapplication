package org.isima.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "event")
public class Event {

    @Id
    @Column(name = "id", nullable = false)
    private int id;

    @Size(max = 15)
    @Column(name = "date", length = 15)
    private int date;

    @Size(max = 20)
    @Column(name = "event", length = 10)
    private String event;

    @Size(max = 2)
    @Column(name = "type", length = 2)
    private String type;

    public String getType()
    {
        return type;
    }
}
