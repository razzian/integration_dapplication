package org.isima.controllers;

import org.isima.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/station4/api/event")
public class EventController {

    @Autowired
    private EventService eventService;


    @GetMapping("/{date}")
    public ResponseEntity isPractible(@PathVariable int date) {
        return ResponseEntity.ok(eventService.isPractible(date));
    }

    @PutMapping("/{date}")
    public ResponseEntity putArriveEvent(@PathVariable int date){
        eventService.putArriveEvent(date);
        return ResponseEntity.ok("event add");
    }

}
