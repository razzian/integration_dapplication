package org.isima.services;

import org.isima.models.Event;
import org.isima.repositories.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventService {

    private int id = 5;
    @Autowired
    private EventRepository eventRepository;

    public boolean isPractible(int date) {
        List<Event> events = eventRepository.getEvent(date);
        boolean practible = true;
        int cour = 0;
        while ((cour < events.size()) && practible) {
            practible = events.get(cour).getType().equals("P");
            cour++;
        }
        return practible;
    }

    public void putArriveEvent(int date) {
        putEvent(date,"Arrivee","P");
    }

    public void putEvent(int date, String event, String type) {
        eventRepository.putEvent(date,event,type,id);
        id++;
    }
}
