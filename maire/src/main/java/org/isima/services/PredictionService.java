package org.isima.services;

import org.isima.models.Event;
import org.isima.repositories.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Service
public class PredictionService {

    @Autowired
    private EventRepository eventRepository;

    public List<String> getPrediction(int date) {

        // name of predicted events
        ArrayList<String> events = new ArrayList<String>();

        // prediction of event "Attaque de rats"
        if(eventRepository.getEvent(date - 3,"Attaque de rats","N").size() != 0)
        {
            events.add("Attaque de rats");
        }
        // prediction of event "Attaque de monstres"
        if(eventRepository.getEvent(date - 5,"Attaque de monstres","N").size() != 0)
        {
            events.add("Attaque de monstres");
        }
        // prediction of event "Pillards"
        if(eventRepository.getEvent(date - 7,"Pillards","N").size() != 0)
        {
            events.add("Pillards");
        }
        // prediction of event "Incendie"
        if(eventRepository.getEvent(date - 15,"Incendie","N").size() != 0)
        {
            events.add("Incendie");
        }
        // event "Arrivee"
        if(eventRepository.getEvent(date,"Arrivee","P").size() != 0)
        {
            events.add("Arrivee");
        }
        return events;
    }


}
