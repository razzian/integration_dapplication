package org.isima.repositories;

import org.isima.models.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long>{

    @Query("select e from Event e where e.date = ?1")
    List<Event> getEvent(int date);

    @Transactional
    @Modifying
    @Query(value = "INSERT into Event (id, date, event, type) values (?4 ,?1 , ?2 , ?3)", nativeQuery = true)
    void putEvent(int date,String event, String type, int id);

    @Query("select e from Event e where e.date = ?1 and e.event = ?2 and e.type = ?3")
    List<Event> getEvent(int date, String event, String type);


}




