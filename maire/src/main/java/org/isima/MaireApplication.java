package org.isima;

import org.isima.services.EventService;
import org.isima.services.PredictionService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@SpringBootApplication
public class MaireApplication {

    private static int dateAjout = 0;
    private static int datePrediction = 0;
    private static String[] items = {"Attaque de rats", "Attaque de monstres", "Pillards", "Incendie"};
    private static String evenement = "Attaque de rats";
    private static String evenementPredi = "Pas de prédiction";


    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(MaireApplication.class).headless(false).run(args);

        final PredictionService predictionService  = context.getBean(PredictionService.class);
        final EventService eventService  = context.getBean(EventService.class);

        JFrame frame = new JFrame("Metro - Application pour les maires");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);

        //Element a afficher
        final JLabel predictionsAffichee = new JLabel();
        predictionsAffichee.setText(evenementPredi);
        JButton boutonAjouter = new JButton("Ajouter l'événement");
        boutonAjouter.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Date " + MaireApplication.dateAjout);
                System.out.println("Event " + MaireApplication.evenement);
                eventService.putEvent(MaireApplication.dateAjout, MaireApplication.evenement, "N");
            }
        });
        JButton boutonPrediction = new JButton("Prédire");
        boutonPrediction.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Date " + MaireApplication.datePrediction);
                List<String> listeEvenements = predictionService.getPrediction(MaireApplication.datePrediction);
                String retour = "";
                for (String evenement: listeEvenements )
                {
                    retour = retour + " - " + evenement ;
                }
                retour += " - ";
                predictionsAffichee.setText(retour);
            }
        });
        final JComboBox comboBox = new JComboBox(items);
        comboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                MaireApplication.evenement = MaireApplication.items[comboBox.getSelectedIndex()];
            }
        });
        final JSpinner spinner = new JSpinner(new SpinnerNumberModel(0,0,365,1));
        ((NumberFormatter)(((JSpinner.DefaultEditor)spinner.getEditor()).getTextField()).getFormatter()).setAllowsInvalid(false);
        spinner.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                MaireApplication.dateAjout = Integer.parseInt(spinner.getValue().toString());
            }
        });
        final JSpinner spinnerPrediction = new JSpinner(new SpinnerNumberModel(0,0,365,1));
        ((NumberFormatter)(((JSpinner.DefaultEditor)spinnerPrediction.getEditor()).getTextField()).getFormatter()).setAllowsInvalid(false);
        spinnerPrediction.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                MaireApplication.datePrediction = Integer.parseInt(spinnerPrediction.getValue().toString());
            }
        });



        //Mise en page
        JPanel panel = new JPanel();
        GridBagLayout layout = new GridBagLayout();
        panel.setLayout(layout);
        GridBagConstraints gbc = new GridBagConstraints();
        //Spinner
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(spinner, gbc);
        //Liste evenement
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 4;
        panel.add(comboBox, gbc);
        //Bouton ajouter
        gbc.gridx = 5;
        gbc.gridy = 0;
        panel.add(boutonAjouter, gbc);
        //Spinner prediction
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        panel.add(spinnerPrediction, gbc);
        //Bouton prediction
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.gridwidth = 4;
        panel.add(boutonPrediction, gbc);
        //Zone de texte pour prédiction
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 10;
        panel.add(predictionsAffichee, gbc);

        frame.add(panel);
        frame.setVisible(true);

    }
}
