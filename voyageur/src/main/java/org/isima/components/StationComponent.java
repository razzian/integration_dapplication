package org.isima.components;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

public class StationComponent {


    public static boolean isDisponible(String station, int date) {

        boolean disponible = false;
        String path = "http://localhost:8080/" + station + "/api/event/" + date ;
        try {
            HttpResponse<String> stringHttpResponse = Unirest.get(path).header("accept", "application/json").asObject(String.class);
            disponible = Boolean.parseBoolean(stringHttpResponse.getBody()) ;

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return disponible;
    }

    public static void putArrive(String station, int date) {

        String path = "http://localhost:8080/" + station + "/api/event/" + date ;
        try {
            Unirest.put(path).header("accept", "application/json").asObject(String.class);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

}
