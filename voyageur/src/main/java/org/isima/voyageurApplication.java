package org.isima;

import org.isima.services.TravelService;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class voyageurApplication {

    static TravelService travelService = new TravelService();
    private static int date = 0;
    private static String[] items = {"station1", "station2", "station3", "station4", "station5", "station6"};
    private static String depart = "station1";
    private static String arrivee = "station1";
    private static String cheminLePLusCourt = "Pas d'itinéraire";


    public static void main(String[] args) {

        JFrame frame = new JFrame("Metro - Application pour les voyageurs");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);

        //Element a afficher
        final JLabel chemin = new JLabel();
        chemin.setText(cheminLePLusCourt);
        JButton boutonGo = new JButton("Calculer");
        boutonGo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> stations =  travelService.goTo(depart,arrivee,date);
                String retour = " - ";
                for (String station: stations )
                {
                    retour = retour + " > " + station ;
                }
                retour += " - ";
                chemin.setText(retour);
            }
        });

        final JComboBox comboBoxDepart = new JComboBox(items);
        comboBoxDepart.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                voyageurApplication.depart = voyageurApplication.items[comboBoxDepart.getSelectedIndex()];
            }
        });

        final JComboBox comboBoxArrivee = new JComboBox(items);
        comboBoxArrivee.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                voyageurApplication.arrivee = voyageurApplication.items[comboBoxArrivee.getSelectedIndex()];
            }
        });
        final JSpinner spinner = new JSpinner(new SpinnerNumberModel(0,0,365,1));
        ((NumberFormatter)(((JSpinner.DefaultEditor)spinner.getEditor()).getTextField()).getFormatter()).setAllowsInvalid(false);
        spinner.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                date = Integer.parseInt(spinner.getValue().toString());
            }
        });

        //Mise en page
        JPanel panel = new JPanel();
        GridBagLayout layout = new GridBagLayout();
        panel.setLayout(layout);
        GridBagConstraints gbc = new GridBagConstraints();
        //Spinner
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 4;
        panel.add(spinner, gbc);
        //Depart
        gbc.gridx = 5;
        gbc.gridy = 0;
        panel.add(comboBoxDepart, gbc);
        //arrivee
        gbc.gridx = 10;
        gbc.gridy = 0;
        panel.add(comboBoxArrivee, gbc);
        //bouton
        gbc.gridx = 10;
        gbc.gridy = 1;
        panel.add(boutonGo, gbc);
        // Texte
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 10;
        panel.add(chemin, gbc);

        frame.add(panel);
        frame.setVisible(true);
    }
}
