package org.isima.services;



import org.isima.components.StationComponent;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultUndirectedGraph;

import java.util.Iterator;
import java.util.List;

public class TravelService {

    private DefaultUndirectedGraph<String, DefaultEdge> stationGraph = new DefaultUndirectedGraph<>(DefaultEdge.class);
    private DefaultUndirectedGraph<String, DefaultEdge> currentStationGraph = new DefaultUndirectedGraph<>(DefaultEdge.class);;
    private StationComponent stationComponent = new StationComponent();


    public TravelService()
    {
        initGraph();
    }

    public List<String> goTo(String startStation, String arriveStation, int date) {
        updateGraph(date);

        DijkstraShortestPath<String, DefaultEdge> dijkstraShortestPath = new DijkstraShortestPath<>(currentStationGraph);
        List<String> way = dijkstraShortestPath.getPath(startStation,arriveStation).getVertexList();
        if(way.size() > 0)
        {
            putArrive(arriveStation, date);
        }
        return way;
    }

    private void initGraph()
    {
        // init default graph of station
        stationGraph.addVertex("station1");
        stationGraph.addVertex("station2");
        stationGraph.addVertex("station3");
        stationGraph.addVertex("station4");
        stationGraph.addVertex("station5");
        stationGraph.addVertex("station6");

        stationGraph.addEdge("station1", "station2");
        stationGraph.addEdge("station2", "station3");
        stationGraph.addEdge("station3", "station6");
        stationGraph.addEdge("station1", "station4");
        stationGraph.addEdge("station4", "station6");
        stationGraph.addEdge("station1", "station5");

        // make a copy for current station graph
        currentStationGraph = (DefaultUndirectedGraph<String, DefaultEdge>)stationGraph.clone();
    }

    private void putArrive(String station, int date)
    {
        stationComponent.putArrive(station, date);
    }

    private void updateGraph(int date)
    {
        currentStationGraph = (DefaultUndirectedGraph<String, DefaultEdge>)stationGraph.clone();

        // right version with all the API
        /*
        Iterator<String> iterator = stationGraph.vertexSet().iterator();
        while (iterator.hasNext()) {
            String currentStation = iterator.next();
            if(!stationComponent.isDisponible(currentStation,date))
            {
                currentStationGraph.removeVertex(currentStation);
            }
        }*/

        // test version with Only the API station4 start
        if(!stationComponent.isDisponible("station4", date))
        {
            currentStationGraph.removeVertex("station4");
        }


    }


}
